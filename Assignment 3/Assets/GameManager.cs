﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour {

    public Text pressAnyKeyToPlay;

    public enum STATES { NONE, GAMEOVER, PLAYING };

    private static int gameState = (int)STATES.NONE;

    public Transform Player;
    public Transform PlayerStartPos;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if(gameState == (int)STATES.GAMEOVER)
        {
            Time.timeScale = 0.0f;
            //pressAnyKeyToPlay.text = "Press any key to play again!";
        }
        else if (gameState == (int)STATES.NONE)
        {
            Time.timeScale = 0.0f;
            pressAnyKeyToPlay.text = "Press any key to play!";
        }

        if (Input.anyKey)
        {
            if( gameState == (int)STATES.NONE /*|| gameState == (int)STATES.GAMEOVER*/)
            {
                // removed play again on gameover -> problems with resetting Invoke timers
                /*if(gameState == (int)STATES.GAMEOVER)
                {
                    Player.transform.position = PlayerStartPos.transform.position;
                    PointsKeeper.setPoints(0);
                    TimeKeeper.setTime(0);
                    PointsKeeper.GameOverText.text = "";

                    Spawner.resetObjects();
                }*/
  
                gameState = (int)STATES.PLAYING;
                Time.timeScale = 1.0f;
                pressAnyKeyToPlay.text = "";
            }
        }

    }

    public static void setGameState(int value)
    {
        gameState = value;
    }
    public static int getGameState()
    {
        return gameState;
    }
}
