﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour {
	public float maxSpeed = 10.0f;
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		Vector2 direction;
		direction.x = Input.GetAxis ("Horizontal");
		direction.y = 0;

		Vector2 velocity = direction * maxSpeed;
		transform.Translate (velocity * Time.deltaTime);
	}
		
}
