using UnityEngine;
using System.Collections;


public class objectsMove : MonoBehaviour {

    public float speed = 5.0f;

	public Transform EndSpawn;

    // Use this for initialization
    void Start () {
       
    }
	
	// Update is called once per frame
	void Update () {
		//Vector2 direction = (Vector2)EndSpawn.position - (Vector2)transform.position;
		//transform.Translate (speed * Time.deltaTime);

		transform.position = Vector2.MoveTowards (transform.position, EndSpawn.transform.position, speed * Time.deltaTime);

	}

	void OnCollisionEnter2D(Collision2D collision)
	{
		
		if (collision.gameObject.tag == "Player") {	
			if (gameObject.tag == "Bombs") {
				//Time.timeScale = 0.0f;
                GameManager.setGameState((int)GameManager.STATES.GAMEOVER);
                PointsKeeper.GameOverText.text = "Game Over!";
                SoundManager.playSound(3);
            } else if(gameObject.tag == "Obst") {
                PointsKeeper.AddPoints(-1);
                SoundManager.playSound(2);
            }
            else {
                PointsKeeper.AddPoints(1);
                SoundManager.playSound(1);
            }
            Destroy(gameObject);
        } else if (collision.gameObject.tag == "EndSpawner") {
			Destroy (gameObject);
		}
	}
}
