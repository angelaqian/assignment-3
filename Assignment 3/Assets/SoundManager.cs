﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class SoundManager : MonoBehaviour {

    public AudioClip pickUpNote;
    public AudioClip pickUpObst;
    public AudioClip pickUpBomb;

    public static AudioClip pickUpNote2;
    public static AudioClip pickUpObst2;
    public static AudioClip pickUpBomb2;

    private static AudioSource audio;

    // Use this for initialization
    void Start () {
        audio = GetComponent<AudioSource>();

        pickUpNote2 = pickUpNote;
        pickUpObst2 = pickUpObst;
        pickUpBomb2 = pickUpBomb;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
    public static void playSound(int Object)
    {
        if(Object == 1)
        {
            audio.PlayOneShot(pickUpNote2);
        }
        else if (Object == 2)
        {
            audio.PlayOneShot(pickUpObst2);
        }
        else
        {
            audio.PlayOneShot(pickUpBomb2);
        }
    }

}
