﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class PointsKeeper : MonoBehaviour {

	private static int points = 0;
    public static Text pointsText;
    public Text pointsTextInput;
    public static Text GameOverText;
    public Text GameOverTextInput; 


	// Use this for initialization
	void Start () {
        pointsText = pointsTextInput;
        GameOverText = GameOverTextInput;
        points = 0;
        UpdatePoints();
       
    }

	public static void AddPoints(int value)
	{
		points += value;
        UpdatePoints();
    }

    public static void UpdatePoints()
    {
        pointsText.text = "Points: " + points;
    }

    public static void setPoints(int value)
    {
        points = value;
    }

    // Update is called once per frame
    void Update () {
	
	}
}
