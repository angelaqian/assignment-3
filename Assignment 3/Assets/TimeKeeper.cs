﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TimeKeeper : MonoBehaviour {
    public static Text timeText;
    public Text timeTextInput;

    private static int time;
    // Use this for initialization
    void Start () {
        timeText = timeTextInput;
        time = 0;
        UpdateTime();
        InvokeRepeating("increase_time", 1, 1);
    }

    void increase_time()
    {
        time++;
        UpdateTime();

        if( time == 120 )
        {
            GameManager.setGameState((int)GameManager.STATES.GAMEOVER);
            PointsKeeper.GameOverText.text = "You WIN!";
        }
    }


	// Update is called once per frame
	static void UpdateTime() {
	    timeText.text = "Time: " + time;
    }

    public static void setTime(int timeSet)
    {
        time = timeSet;
        UpdateTime();
    }
}
