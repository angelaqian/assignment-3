﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {
	public objectsMove[] Prefabs;
	public Transform[] StartSpawn;
	public Transform[] EndSpawn;
    public GameObject parentLoc;
    private static GameObject parentLoc2;

	public float timer = 3f;
	private float decreaseAmount = 0.5f;
    private static float defaultDecreaseInterval = 10f;
	private static float decreaseInterval = 10f;
	private float nextNote;
	private bool waitingNote;

	private float gameTime;


	private bool allowBombs = false;
	private bool allowObst = false;

   

	// Use this for initialization
	void Start () {
        parentLoc2 = parentLoc;
        InvokeRepeating ("descreaseTimer", 5, decreaseInterval);
		Invoke ("timer_allow_bomb", 30);
		Invoke ("timer_allow_obst", 15);
	}


	void timer_allow_bomb()
	{
		allowBombs = true;
	}

	void descreaseTimer()
	{
		if (timer > .5f) {
			timer -= decreaseAmount;
			decreaseInterval += 0.5f;

			//Debug.Log ("Timer is at " + timer);
		} else
			timer = 0.5f;
	}

	void timer_allow_obst()
	{
		allowObst = true;
	}


	// Update is called once per frame
	void Update () {
		if (!waitingNote && GameManager.getGameState() == (int)GameManager.STATES.PLAYING) {
			int spawnIndex = Random.Range (0, StartSpawn.Length);
			int objectIndex = 0;

			float randomValue = Random.value;

			if (randomValue < 0.1 && allowBombs) {
				objectIndex = 1; 
			} else if (randomValue < 0.3 && allowObst) {
				objectIndex = 2;
			} else {
				objectIndex = 0;
			}
			objectsMove things = Instantiate (Prefabs[objectIndex]);
			things.transform.parent = transform;
			things.gameObject.name = "object";

			things.transform.position = StartSpawn [spawnIndex].position;
			things.EndSpawn = EndSpawn [spawnIndex];
            things.transform.parent = parentLoc.transform;

            waitingNote = true;
			nextNote = Time.time + timer;

		}

		if (waitingNote && Time.time >= nextNote)
			waitingNote = false;
	}

    public static void resetObjects()
    {
        foreach (Transform child in parentLoc2.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        decreaseInterval = defaultDecreaseInterval;
    }

}
